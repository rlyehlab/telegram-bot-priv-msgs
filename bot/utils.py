from bot.env import try_get

admin_group = int(try_get('ADMIN_GROUP'))

def get_username(update):
    return update.effective_user.username

def get_user_id(update):
    return update.effective_user.id

def escape_markdown_entities(text):
    return text \
        .replace('\\', '\\\\') \
        .replace('.', '\.') \
        .replace('(', '\(') \
        .replace(')', '\)') \
        .replace('=', '\=') \
        .replace('_', '\_') \
        .replace('-', '\-') \
        .replace('*', '\*')
