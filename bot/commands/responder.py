from telegram.ext import (
    ConversationHandler,
    CallbackQueryHandler,
    MessageHandler,
    Filters,
    CommandHandler,
)

from bot.utils import get_user_id, admin_group
from .cancel import cancel

def create_handler():
    return ConversationHandler(
        entry_points=[CallbackQueryHandler(entry_point)],
        states={
            'RESPONDER_0':[
                MessageHandler((Filters.text & ~Filters.command & Filters.chat(admin_group)), respuesta),
            ],
        },
        fallbacks=[CommandHandler('cancelar', cancel)],
    )

def entry_point(update, context):
    query = update.callback_query
    query.answer()
    callback_data = query.data

    user_id = callback_data
    context.chat_data['user_id'] = user_id

    reply_text = '''⏺ Muy bien, ahora escriban la respuesta (o cancelen con /cancelar).
⚠️ Cuidado que cualquier texto enviado a este chat será tomado como la respuesta!'''

    context.bot.send_message(
        chat_id=admin_group,
        text=reply_text,
    )

    return 'RESPONDER_0'

def respuesta(update, context):
    # si usamos text_markdown_v2 a secas no se arman previews de los links
    # text_markdown_v2 - https://python-telegram-bot.readthedocs.io/en/stable/telegram.message.html#telegram.Message.text_markdown_v2
    admin_msg = update.message.text_markdown_v2_urled

    user_id = context.chat_data['user_id']
    del context.chat_data['user_id']

    context.bot.send_message(
        chat_id=user_id,
        text=admin_msg,
        parse_mode='MarkdownV2',
    )

    update.message.reply_text('✅ Mensaje enviado!', quote=False)

    return ConversationHandler.END
