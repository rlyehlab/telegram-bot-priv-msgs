from telegram.ext import (
    ConversationHandler,
    CommandHandler,
    MessageHandler,
    Filters,
)

from bot.utils import admin_group
from .cancel import cancel

def create_handler():
    # https://github.com/python-telegram-bot/python-telegram-bot/blob/master/examples/conversationbot.py
    # Add conversation handler
    return ConversationHandler(
        entry_points=[CommandHandler('mensaje_directo', entry_point, filters=Filters.chat(admin_group))],
        states={
            'MENSAJE_DIRECTO_0': [MessageHandler((Filters.text & ~Filters.command & Filters.chat(admin_group)), id_usuarix)],
            'MENSAJE_DIRECTO_1': [MessageHandler((Filters.text & ~Filters.command & Filters.chat(admin_group)), mandar_mensaje)],
        },
        fallbacks=[CommandHandler('cancelar', cancel)],
    )

def entry_point(update, context):
    reply_text = f'⏺ Muy bien, ahora escribanme el id de lx usuarix (o cancelen con /cancelar):'
    update.message.reply_text(reply_text, quote=False)

    return 'MENSAJE_DIRECTO_0'

def id_usuarix(update, context):
    user_id = update.message.text
    context.chat_data['user_id'] = user_id

    reply_text = '''⏺ Perfecto! Ahora escribanme el mensaje a enviar (o cancelen con /cancelar).
⚠️ Cuidado que cualquier texto enviado a este chat será tomado como el mensaje!'''
    update.message.reply_text(reply_text, quote=False)

    return 'MENSAJE_DIRECTO_1'

def mandar_mensaje(update, context):
    # si usamos text_markdown_v2 a secas no se arman previews de los links
    # text_markdown_v2 - https://python-telegram-bot.readthedocs.io/en/stable/telegram.message.html#telegram.Message.text_markdown_v2
    admin_msg = update.message.text_markdown_v2_urled

    user_id = context.chat_data['user_id']
    del context.chat_data['user_id']

    context.bot.send_message(
        chat_id=user_id,
        text=admin_msg,
        parse_mode='MarkdownV2',
    )

    update.message.reply_text('✅ Mensaje enviado!', quote=False)

    return ConversationHandler.END
