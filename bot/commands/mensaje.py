from telegram.ext import (
    MessageHandler,
    Filters,
)
from telegram import InlineKeyboardButton, InlineKeyboardMarkup

from bot.utils import get_user_id, get_username, escape_markdown_entities, admin_group

def create_handler():
    return MessageHandler((Filters.text & ~Filters.command & Filters.chat_type.private), command)

separator = '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - '
def command(update, context):
    user_id = get_user_id(update)
    username = get_username(update)
    if username: username = f'@{username}'
    else: username = update.effective_user.full_name

    # si usamos text_markdown_v2 a secas no se arman previews de los links
    # text_markdown_v2 - https://python-telegram-bot.readthedocs.io/en/stable/telegram.message.html#telegram.Message.text_markdown_v2
    user_msg = update.message.text_markdown_v2_urled

    admin_msg = escape_markdown_entities(f'📩 {username} (id={user_id}) ha enviado el siguiente mensaje:\n{separator}\n')
    admin_msg += user_msg
    admin_msg += escape_markdown_entities(f'\n{separator}\nPresionen responder cuando esten listxs para responder')

    # InlineKeyboardButton - https://python-telegram-bot.readthedocs.io/en/stable/telegram.inlinekeyboardbutton.html
    keyboard = [
        [InlineKeyboardButton("Responder", callback_data=user_id)],
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)

    context.bot.send_message(
        chat_id=admin_group,
        text=admin_msg,
        parse_mode='MarkdownV2',
        disable_web_page_preview=True,
        reply_markup=reply_markup
    )

    update.message.reply_text('✅ Mensaje enviado!', quote=False)
