import pprint

from telegram.ext import (
    CommandHandler,
    Filters,
)

from bot.utils import admin_group

def create_handler():
    return CommandHandler("debug", command, filters=Filters.chat(admin_group))

def command(update, context):
    # ocultamos propiedades privadas _PROP o __PROP
    update_debug = pprint.pformat([p for p in dir(update) if p[0] != '_'])
    context_debug = pprint.pformat([p for p in dir(context) if p[0] != '_'])
    chat_id = update.effective_chat.id
    chat_name = update.effective_chat.username
    user_id = update.effective_user.id
    user_name = update.effective_user.username
    message_text = update.effective_message.text

    #full_debug = f'update:\n{update_debug}\n\n' \
    #    f'context:\n{context_debug}\n\n' \
    full_debug = f'update id={update.update_id}\n' \
        f'chat id={chat_id} username={chat_name}\n' \
        f'user id={user_id} username={user_name}\n' \
        f'admin_group={admin_group}\n' \
        f'message text={message_text}'

    # imprimimos en consola
    print(full_debug)

    # y devolvemos como mensaje
    update.message.reply_text(full_debug)

    # otros prints útiles. cambiar vars por dir para más data (pero sin valores)
    #pprint.pprint(vars(update))
    #pprint.pprint(vars(context))
    # datos del user/config del bot
    #pprint.pprint(vars(context.bot.bot))
    #pprint.pprint(vars(update.effective_user))
    #pprint.pprint(vars(update.effective_message))
    #pprint.pprint(vars(update.effective_chat))
