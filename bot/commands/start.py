from telegram.ext import CommandHandler

def create_handler():
    return CommandHandler("start", command)

def command(update, context):
    # no tiene tabs/espacios el texto porque sino los incluye en el mensaje final
    reply_text = '''Hola!

Este bot reenviará tus mensajes a un grupo privado especial del hacklab para \
que podamos responderte de manera colectiva y organizada.

Mandanos tus mensajes y aguarda y serás atendidx por este mismo chat :)'''

    update.message.reply_text(reply_text, disable_web_page_preview=True)
