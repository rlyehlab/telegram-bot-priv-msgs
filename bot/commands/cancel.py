from telegram.ext import ConversationHandler

def cancel(update, context):
    update.message.reply_text('✴️ Comando cancelado', quote=False)
    return ConversationHandler.END
