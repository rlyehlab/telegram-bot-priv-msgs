__Bot de Telegram para chatear por privado colectivamente__

Este bot está basado en el código de https://gitlab.com/saius/telegram-bot-mensajeador/

### Funcionalidades

Este bot recibe mensajes privados de cualquier persona y los reenvía a un grupo admin.
Desde ese grupo cualquiera puede responder al mensaje o enviar mensajes nuevos a cualquier
usuarix, siempre y cuando se tengo su user id.

Los comandos (en formato para pasarle a [@BotFather](https://telegram.me/botfather)) son:
> start - Comenzar a chatear con el bot   
> mensaje_directo - Enviar mensaje directo a usuarix [solo admin]   
> cancelar - Cancela el comando actual [solo admin]   
> debug - Datos de debug del bot [solo admin]   

Viene con un watch-er de cambios de código para recargar el bot automáticamente.

### Instalación

1. Crear venv si no está creado `python3 -m venv venv`
1. Activar el venv si no estaba activado `source venv/bin/activate`
1. Instalar dependencia `pip install -r requirements.txt`
1. Copiar `.env.example` a `.env` y cambiar variables acorde a su caso
1. Ejecutar `python3 bot.py`, o `python3 watcher.py` si lo desean correr con
en modo recarga automática de cambios.

Después solo hará falta ejecutar el último comando listado para iniciar el bot.

### Configuración

Variables del `.env` son:
- `BOT_TOKEN` es el token del bot, en el formato NÚMERO:LETRAS_Y_SÍMBOLOS
- `LOGGING` settea el logging interno: 1 o yes para loggear, 0 o no para no loggear
- `ADMIN_GROUP` el id numérico del grupo al cual el bot reenviará los mensajes y desde el cual se responderan

### Docker

Para correr en docker primero build-ear la imagen:

`docker build -t telegram-bot-priv-msgs .`

Y después correrla pasándole los parámetros correspondientes:

`docker run --rm -eBOT_TOKEN=bot_token -eLOGGING=1 -eADMIN_GROUP=-1234567890 telegram-bot-priv-msgs`
